﻿import { statusCode } from '../Utilities';

export function login(userId, password) {
    return new Promise((resolve, reject) => {
        var data = {
            UserId: userId,
            Password: password
        };

        console.log("Starting API call to Login()");
        var logMessage = "Response from API Login(): ";

        fetch("user/login/authenticate", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json()
            .then(data => ({ status: response.status, body: data })))
        .then(obj => {
            console.log(logMessage + statusCode(obj.status));

            // Login successful
            if (obj.status === 200) {
                resolve({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages,
                    user: obj.body.user
                });
            }

            // Login failed
            if (obj.status === 500) {
                reject({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
    });
}

export function checkLogin() {
    return new Promise((resolve, reject) => {

        console.log("Starting API call to CheckLogin()");
        var logMessage = "Response from API CheckLogin(): ";

        fetch("user/login/check", {
            method: "get"
        })
        .then(response => {
            console.log(logMessage + statusCode(response.status));

            // User is already logged in
            if (response.status === 200) {
                resolve({
                    status: response.status
                });
            }

            // User is not logged in
            if (response.status === 401) {
                reject({
                    status: response.status
                });
            }
        })
        .catch(error => {
            console.error(error)
        });
    });
}

export function logout() {
    return new Promise((resolve, reject) => {

        console.log("Starting API call to Logout()");
        var logMessage = "Response from API Logout(): ";

        fetch("user/login/logout", {
            method: "get"
        })
        .then(response => {
            console.log(logMessage + statusCode(response.status));

            // Logout success
            if (response.status === 200) {
                resolve({
                    status: response.status
                });
            }

            // Logout failure
            if (response.status === 500) {
                reject({
                    status: response.status
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
    });
}