﻿import React, { Component } from 'react';

export class Account extends Component {
    static displayName = Account.name;

    render() {
        return (
            <div>
                <h1>Account</h1>
                <p>Hello [name], welcome back!</p>
                <h2>Add Job</h2>
                <p>Add the table component thingy here</p>
            </div>
        );
    }
}