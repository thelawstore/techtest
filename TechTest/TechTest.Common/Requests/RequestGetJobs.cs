﻿namespace TechTest.Common.Requests
{
    /// <summary>
    /// Retrieves all jobs from DB.
    /// This is just an empty request as no validation or search criteria
    /// is required.
    /// </summary>
    public class RequestGetJobs: Request
    {
    }
}
