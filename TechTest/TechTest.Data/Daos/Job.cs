﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TechTest.Data.Daos
{
    public partial class Job
    {
        public int JobId { get; set; }
        public int JobTypeId { get; set; }
        public decimal Salary { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Posted { get; set; }

        public virtual JobType JobType { get; set; }
    }
}
