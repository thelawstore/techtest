﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace TechTest.Common
{
    /// <summary>
    /// Common application settings.
    /// </summary>
    public class AppSettings
    {
        // Get the database connection string.
        public string ConnectionString { get; }

        public AppSettings()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();

            ConnectionString = root.GetConnectionString("Database");
        }
    }
}
