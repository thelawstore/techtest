﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TechTest.Services;
using TechTest.Common.Requests;
using TechTest.Common.Responses;

// Security
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;


namespace TechTest.Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// Check that user credentials entered by the user match the credentials
        /// in the database. Does not require role permissions; for this example,
        /// we're assuming all logged-in users are admins.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login/authenticate")]
        public async Task<IActionResult> Login([FromBody] RequestGetUserLogin request)
        {
            var service = new UserService();
            var response = service.GetUserLogin(request);

            if (response.Status == HttpStatusCode.OK)
            {
                var claims = new List<Claim>();

                // Add the user details to the authetnication claim
                claims.Add(new Claim(ClaimTypes.NameIdentifier, response.User.UserId));
                claims.Add(new Claim(ClaimTypes.GivenName, response.User.FirstName));
                claims.Add(new Claim(ClaimTypes.Surname, response.User.LastName));

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    AllowRefresh = true,
                    // Refreshing the authentication session should be allowed.

                    //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    // The time at which the authentication ticket expires. A 
                    // value set here overrides the ExpireTimeSpan option of 
                    // CookieAuthenticationOptions set with AddCookie.

                    IsPersistent = true,
                    // Whether the authentication session is persisted across 
                    // multiple requests. When used with cookies, controls
                    // whether the cookie's lifetime is absolute (matching the
                    // lifetime of the authentication ticket) or session-based.

                    IssuedUtc = DateTime.Now,
                    // The time at which the authentication ticket was issued.

                    //RedirectUri = <string>
                    // The full path or absolute URI to be used as an http 
                    // redirect response value.
                };

                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties
                );
            }
            return StatusCode((int)response.Status, response);
        }

        /// <summary>
        /// Checks if the user is authenticated. For security, in a real system,
        /// this method should probably check that the user account hasn't been
        /// disabled in the database while the user is already logged in.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("login/check")]
        public IActionResult CheckLogin()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return Ok();
            }
            return Unauthorized();
        }

        /// <summary>
        /// Terminate the login session. This method should not be callable if
        /// the user is not authenticated.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("login/close")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
