﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TechTest.Data.Daos
{
    public partial class User
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
