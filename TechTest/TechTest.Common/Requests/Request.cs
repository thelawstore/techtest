﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using TechTest.Common.Responses;


namespace TechTest.Common.Requests
{
    public abstract class Request: IValidatableObject
    {
        /// <summary>
        /// Validate annotated properties in subclass.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public Response CheckValidation(Response response)
        {
            List<ValidationResult> validationResults = new();
            ValidationContext validationContext = new(this);
            bool isValid = Validator.TryValidateObject(this, validationContext, validationResults, true);

            if (!isValid)
            {
                response.Status = HttpStatusCode.BadRequest;

                foreach (ValidationResult result in validationResults)
                    response.StatusMessages.Add(new StatusMessage(HttpStatusCode.BadRequest, result.ErrorMessage));
            }

            return response;
        }

        /// <summary>
        /// Override for more complex validation.
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            return results;
        }
    }
}
