﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TechTest.Data.Daos
{
    public partial class DbContextTechTest : DbContext
    {
        public DbContextTechTest()
        {
        }

        public DbContextTechTest(DbContextOptions<DbContextTechTest> options)
            : base(options)
        {
        }

        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobType> JobTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("server=localhost;database=tlstechtest;user=root");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("Job");

                entity.HasIndex(e => e.JobTypeId, "JobTypeID");

                entity.Property(e => e.JobId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("JobID");

                entity.Property(e => e.Description).HasColumnType("varchar(8192)");

                entity.Property(e => e.JobTypeId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("JobTypeID");

                entity.Property(e => e.Posted).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Salary).HasColumnType("decimal(10,0)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.HasOne(d => d.JobType)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.JobTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("job_ibfk_1");
            });

            modelBuilder.Entity<JobType>(entity =>
            {
                entity.ToTable("JobType");

                entity.Property(e => e.JobTypeId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("JobTypeID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.UserId)
                    .HasMaxLength(32)
                    .HasColumnName("UserID");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
