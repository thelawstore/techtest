using System;
using NUnit.Framework;
using TechTest.Common.Requests;
using System.Net;

namespace TechTest.Services.Testing
{
    public class JobTesting
    {
        [SetUp]
        public void Setup()
        {
        }

        // These aren't good unit tests; I just want to use them for a quick
        // debug run to make sure the back end is working as expected.

        [Test]
        public void TestGetJobs()
        {
            var service = new JobService();

            var request = new RequestGetJobs();

            var response = service.GetJobs(request);

            Assert.AreEqual(response.Status, HttpStatusCode.OK);
        }

        [Test]
        public void TestGetJobsTypes()
        {
            var service = new JobService();

            var request = new RequestGetJobTypes();

            var response = service.GetJobTypes(request);

            Assert.AreEqual(response.Status, HttpStatusCode.OK);
        }

        [Test]
        public void TestAddJob()
        {
            var service = new JobService();

            var random = new Random();

            var request = new RequestAddJob()
            {
                JobTypeId = 1,
                Salary = 100000,
                Title = "Test Job " + random.Next(1, 100000),
                Description = "This is a test job generated in unit testing."
            };

            var response = service.AddJob(request);

            Assert.AreEqual(response.Status, HttpStatusCode.OK);
        }
    }
}