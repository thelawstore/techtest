﻿using System;
using NUnit.Framework;
using TechTest.Common.Requests;
using System.Net;

namespace TechTest.Services.Testing
{
    public class UserTesting
    {
        [SetUp]
        public void Setup()
        {
        }

        // These aren't good unit tests; I just want to use them for a quick
        // debug run to make sure the back end is working as expected.

        /// <summary>
        /// Test user login. Assumes admin user has already been created in DB.
        /// </summary>
        [Test]
        public void TestGetUserLogin()
        {
            var service = new UserService();

            // Test valid login
            var request = new RequestGetUserLogin()
            {
                UserId = "admin",
                Password = "NoHack4U"
            };

            var response = service.GetUserLogin(request);

            Assert.AreEqual(response.Status, HttpStatusCode.OK);

            // Test invalid login
            request = new RequestGetUserLogin()
            {
                UserId = "admin",
                Password = "Password1"
            };

            response = service.GetUserLogin(request);

            Assert.AreEqual(response.Status, HttpStatusCode.InternalServerError);
        }
    }
}