﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TechTest.Services;
using TechTest.Common.Requests;
using TechTest.Common.Responses;

// Security
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TechTest.Web.Controllers
{
    [Route("api/[controller]")]
    public class JobController : ControllerBase
    {
        /// <summary>
        /// Get a collection of all available jobs stored in the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IActionResult GetJobs()
        {
            var request = new RequestGetJobs();
            var service = new JobService();
            var response = service.GetJobs(request);

            return StatusCode((int)response.Status, response);
        }

        /// <summary>
        /// Get a collection of jobs containing the search string entered by the
        /// user.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("title")]
        public IActionResult GetJobsByTitle(RequestGetJobsByTitle request)
        {
            var service = new JobService();
            var response = service.GetJobsByTitle(request);

            return StatusCode((int)response.Status, response);
        }

        /// <summary>
        /// Add the job that the user submitted in the form to the database.
        /// This method requires authentication.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize]
        public IActionResult AddJob([FromBody] RequestAddJob request)
        {
            var service = new JobService();
            var response = service.AddJob(request);

            return StatusCode((int)response.Status, response);
        }
    }
}
