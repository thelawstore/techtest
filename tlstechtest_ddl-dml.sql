DROP DATABASE IF EXISTS tlstechtest;
CREATE DATABASE tlstechtest;
USE tlstechtest;

-- --- --
-- DDL --
-- --- --

CREATE TABLE User (
	UserID VARCHAR(32) NOT NULL,
    Password VARCHAR(128) NOT NULL,
    FirstName VARCHAR(32) NOT NULL,
    LastName VARCHAR(32) NOT NULL,
    PRIMARY KEY (UserID)
);

CREATE TABLE JobType (
	JobTypeID INT UNSIGNED AUTO_INCREMENT,
    Name VARCHAR(32) NOT NULL,
    PRIMARY KEY (JobTypeID)
);

CREATE TABLE Job (
	JobID INT UNSIGNED AUTO_INCREMENT,
    JobTypeID INT UNSIGNED NOT NULL,
    Salary DECIMAL NOT NULL,
    Title VARCHAR(32) NOT NULL,
    Description VARCHAR(8192),
    Posted DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (JobID),
    FOREIGN KEY (JobTypeID) REFERENCES JobType(JobTypeID)
);


-- --- --
-- DML --
-- --- --

-- Add default user (admin)

-- Password: NoHack4U
INSERT INTO User (UserID, Password, FirstName, LastName)
VALUES ('admin', 'd0f95cc18ff4ef8f2ba1e586e2e7640f204a4ed56770e109ce79c61553fb84299081bf082d1bca83e0e72e0bd9003afa65efc304e0c796041fb2ea555814f936', 'Gavin', 'Kilbride');


-- Define job types

INSERT INTO JobType (Name)
VALUES ('Front End');

INSERT INTO JobType (Name)
VALUES ('Back End');

INSERT INTO JobType (Name)
VALUES ('Full Stack');


-- Add some jobs

INSERT INTO Job (JobTypeID, Salary, Title, Description)
VALUES (1, 120000, 'Senior Front End Developer', 'All about the job');

INSERT INTO Job (JobTypeID, Salary, Title, Description)
VALUES (2, 150000, 'Lead Back End Developer', 'All about the job');

INSERT INTO Job (JobTypeID, Salary, Title, Description)
VALUES (3, 70000, 'Junior Full Stack Developer', 'All about the job');

INSERT INTO Job (JobTypeID, Salary, Title, Description)
VALUES (3, 160000, 'Lead Full Stack Developer', 'All about the job');

INSERT INTO Job (JobTypeID, Salary, Title, Description)
VALUES (1, 60000, 'Graduate Front End Developer', 'All about the job');