import React, { Component } from 'react';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <h1>Home</h1>
                <p>Andrew Hill's tech test for thelawstore.com.au.</p>
            </div>
        );
    }
}