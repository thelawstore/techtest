﻿import { statusCode } from '../Utilities';

export function addJob(jobTypeId, salary, title, description) {
    return new Promise((resolve, reject) => {

        var data = {
            JobTypeId: jobTypeId,
            Salary: salary,
            Title: title,
            Description: description
        };

        console.log("Starting API call to AddJob()");
        var logMessage = "Response from API AddJob(): ";

        fetch("job", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json()
            .then(data => ({ status: response.status, body: data })))
        .then(obj => {

            console.log(logMessage + statusCode(obj.status));

            // Add Job Successful
            if (obj.status === 200) {
                resolve({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages
                });
            }

            // Add Job Failed
            if (obj.status !== 200) {
                reject({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
    });
}

export function getJobs() {
    return new Promise((resolve, reject) => {

        console.log("Starting API call to GetJobs()");
        var logMessage = "Response from API GetJobs(): ";

        fetch("api/job", {
            method: "get"
        })
        .then(response => response.json()
            .then(data => ({ status: response.status, body: data })))
        .then(obj => {
            console.log(logMessage + statusCode(obj.status));

            //alert(JSON.stringify(obj, null, 4)); // Show JSON contents.

            // Get Jobs Successful
            if (obj.status === 200) {
                resolve({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages,
                    jobs: obj.body.jobs
                });
            }

            // Get Jobs Failed
            if (obj.status !== 200) {
                reject({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
    });
}

export function getJobsByTitle(title = "") {
    return new Promise((resolve, reject) => {

        console.log("Starting API call to GetJobsByTitle()");
        var logMessage = "Response from API GetJobsByTitle(): ";

        fetch("api/job/title" + title, {
            method: "get"
        })
        .then(response => response.json()
            .then(data => ({ status: response.status, body: data })))
        .then(obj => {
            console.log(logMessage + statusCode(obj.status));

            // Get Jobs Successful
            if (obj.status === 200) {
                resolve({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages,
                    jobs: obj.body.jobs
                });
            }

            // Get Jobs Failed
            if (obj.status !== 200) {
                reject({
                    status: obj.status,
                    statusMessages: obj.body.statusMessages
                });
            }
        })
        .catch(error => {
            console.error(error);
        });
    });
}