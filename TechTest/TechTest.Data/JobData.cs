﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TechTest.Common.Requests;
using TechTest.Data.Daos;

namespace TechTest.Data
{
    public class JobData
    {
        /// <summary>
        /// Get all job from the database, including associated job type.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<Job> GetJobs(RequestGetJobs request)
        {
            List<Job> jobs = new();

            using (var context = new DbContextTechTest())
            {
                jobs = context.Jobs.Include("JobType").ToList();
            }

            return jobs;
        }

        /// <summary>
        /// Return list of jobs matching provided search string.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<Job> GetJobsByTitle(RequestGetJobsByTitle request)
        {
            List<Job> jobs = new();

            using (var context = new DbContextTechTest())
            {
                jobs = context.Jobs.Include("JobType")
                    .Where(j => j.Title.Contains(request.Title))
                    .ToList();
            }

            return jobs;
        }


        /// <summary>
        /// Get all job types from the database.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<JobType> GetJobTypes(RequestGetJobTypes request)
        {
            List<JobType> jobTypes = new();

            using (var context = new DbContextTechTest())
            {
                jobTypes = context.JobTypes.ToList();
            }

            return jobTypes;
        }

        public void AddJob(RequestAddJob request)
        {
            using (var context = new DbContextTechTest())
            {
                var job = new Job()
                {
                    JobTypeId = request.JobTypeId,
                    Salary = request.Salary,
                    Title = request.Title,
                    Description = request.Description,
                    Posted = DateTime.Now
                };

                context.Add(job);
                context.SaveChanges();
            }
        }
    }
}
