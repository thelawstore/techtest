﻿using TechTest.Data.Daos;

namespace TechTest.Services.Dtos
{
    public class UserDto
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        /// <summary>
        /// Default constructor. Takes no arguments.
        /// </summary>
        public UserDto()
        {
        }

        /// <summary>
        /// Overloaded constructor that takes a User object and converts it to
        /// a UserDto object.
        /// </summary>
        /// <param name="user"></param>
        public UserDto(User user)
        {
            UserId = user.UserId;
            Password = ""; // Password should never be sent to front.
            FirstName = user.FirstName;
            LastName = user.LastName;
        }

        /// <summary>
        /// Converts this UserDto to a User object and returns it.
        /// </summary>
        /// <returns></returns>
        public User ToDao()
        {
            var result = new User()
            {
                UserId = this.UserId,
                Password = this.Password,
                FirstName = this.FirstName,
                LastName = this.LastName
            };

            return result;
        }
    }
}
