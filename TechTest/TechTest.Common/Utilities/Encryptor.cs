﻿using System.Text;
using System.Security.Cryptography;

namespace TechTest.Common.Utilities
{
    public class Encryptor
    {
        // https://stackoverflow.com/questions/12416249/hashing-a-string-with-sha256

        public static string Encrypt(string unencrypted)
        {
            var crypt = new SHA512Managed();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(unencrypted));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}