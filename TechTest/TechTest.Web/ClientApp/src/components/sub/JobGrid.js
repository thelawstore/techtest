﻿import React, { Component } from 'react';
import { getJobs, getJobsByTitle } from '../../calls/JobCalls';

export class JobGrid extends Component {

    constructor(props) {
        super(props);
        this.state = {
            jobs: [],
            jobFetchComplete: false
        };
    }

    componentDidMount() {
        this.getJobs();
    }

    getJobs() {
        // Call fetch to API
        getJobs()
        .then((response) => {
            this.setState({
                jobs: response.jobs,
                jobFetchComplete: true
            });
        })
        .catch((error) => {
            console.error("Error getting jobs: " + error.status);
        });
    }

    renderJobTable = (job, index) => {
        return (
            <tr key={index}>
                <td>{job.jobType.name}</td>
                <td>{job.salary}</td>
                <td>{job.title}</td>
                <td>{job.description}</td>
            </tr>
        )
    }

    render() {
        if (this.state.jobFetchComplete === false) {
            return (<p>Loading...</p>);
        }

        //alert(JSON.stringify(this.state.jobs, null, 4)); // Show JSON contents.

        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Job Type</th>
                            <th>Salary</th>
                            <th>Title</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.jobs.map(this.renderJobTable)}
                    </tbody>
                </table>
            </div>
        );
    }
}