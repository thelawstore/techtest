﻿using System;
using System.Net;
using System.Collections.Generic;
using TechTest.Common.Requests;
using TechTest.Common.Responses;
using TechTest.Services.Responses;
using TechTest.Data;
using TechTest.Data.Daos;
using TechTest.Services.Dtos;


namespace TechTest.Services
{
    public class JobService
    {
        public ResponseGetJobs GetJobs(RequestGetJobs request)
        {
            var response = new ResponseGetJobs();

            try
            {
                // Call data access.
                var data = new JobData();
                var jobDaos = data.GetJobs(request);

                // Convert Database entities for front end.
                List<JobDto> jobDtos = new();
                foreach (Job j in jobDaos)
                {
                    jobDtos.Add(new JobDto(j));
                }

                // Append list of jobs
                response.Jobs = jobDtos;

                // Data retrieval success, attach status messages.
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved jobs."));
            }
            catch (Exception ex)
            {
                // Data retrieval failed.
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public ResponseGetJobsByTitle GetJobsByTitle(RequestGetJobsByTitle request)
        {
            var response = new ResponseGetJobsByTitle();

            try
            {
                // Call data access.
                var data = new JobData();
                var jobDaos = data.GetJobsByTitle(request);

                // Convert Database entities for front end.
                List<JobDto> jobDtos = new();
                foreach (Job j in jobDaos)
                {
                    jobDtos.Add(new JobDto(j));
                }

                // Append list of jobs
                response.Jobs = jobDtos;

                // Data retrieval success, attach status messages.
                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved jobs."));
            }
            catch (Exception ex)
            {
                // Data retrieval failed.
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public ResponseGetJobTypes GetJobTypes(RequestGetJobTypes request)
        {
            var response = new ResponseGetJobTypes();

            try
            {
                var data = new JobData();
                var jobTypeDaos = data.GetJobTypes(request);

                List<JobTypeDto> jobTypeDtos = new();
                foreach (JobType t in jobTypeDaos)
                {
                    jobTypeDtos.Add(new JobTypeDto(t));
                }

                response.JobTypes = jobTypeDtos;

                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully retrieved job types."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }

        public ResponseAddJob AddJob(RequestAddJob request)
        {
            var response = new ResponseAddJob();

            // Validate the input data before calling data layer.
            response = (ResponseAddJob)request.CheckValidation(response);

            // If the validation fails, return the response now.
            if (response.Status == HttpStatusCode.BadRequest)
                return response;

            try
            {
                var data = new JobData();

                data.AddJob(request);

                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "Successfully added job."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
