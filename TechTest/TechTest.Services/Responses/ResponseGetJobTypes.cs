﻿using System.Collections.Generic;
using TechTest.Common.Responses;
using TechTest.Services.Dtos;

namespace TechTest.Services.Responses
{
    public class ResponseGetJobTypes: Response
    {
        public List<JobTypeDto> JobTypes { get; set; }
    }
}
