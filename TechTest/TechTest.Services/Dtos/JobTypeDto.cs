﻿using TechTest.Data.Daos;

namespace TechTest.Services.Dtos
{
    public class JobTypeDto
    {
        public int JobTypeId { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Default constructor. Takes no arguments.
        /// </summary>
        public JobTypeDto()
        {
        }

        /// <summary>
        /// Overloaded constructor that takes a JobType object and converts it to
        /// a JobTypeDto object.
        /// </summary>
        /// <param name="jobType"></param>
        public JobTypeDto(JobType jobType)
        {
            JobTypeId = jobType.JobTypeId;
            Name = jobType.Name;
        }

        /// <summary>
        /// Converts this JobTypeDto to a JobType object and returns it.
        /// </summary>
        /// <returns></returns>
        public JobType ToDao()
        {
            var result = new JobType()
            {
                JobTypeId = JobTypeId,
                Name = Name
            };

            return result;
        }
    }
}
