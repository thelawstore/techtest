﻿using System.Collections.Generic;
using System.Net;

namespace TechTest.Common.Responses
{
    /// <summary>
    /// Used to handle status codes and messages returned from validation of
    /// Request objects. Subclasses handle return of data.
    /// </summary>
    public abstract class Response
    {
        public Response()
        {
            StatusMessages = new List<StatusMessage>();
        }

        public List<StatusMessage> StatusMessages { get; set; }

        public HttpStatusCode Status = HttpStatusCode.InternalServerError;
    }

    /// <summary>
    /// Individual messages to append to Response objects.
    /// </summary>
    public class StatusMessage
    {
        public HttpStatusCode MessageStatus { get; set; }

        public string Message { get; set; }

        public StatusMessage(HttpStatusCode statusCode, string message)
        {
            MessageStatus = statusCode;
            Message = message;
        }
    }
}
