﻿export function statusCode(code) {
    if (code === 200) return code + " (OK)";
    if (code === 400) return code + " (Bad Request)";
    if (code === 401) return code + " (Unauthorized)";
    if (code === 403) return code + " (Forbidden)";
    if (code === 500) return code + " (Internal Server Error)";
    return code + "(UNEXPECTED)";
}