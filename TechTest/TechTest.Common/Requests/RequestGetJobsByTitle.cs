﻿namespace TechTest.Common.Requests
{
    public class RequestGetJobsByTitle: Request
    {
        public string Title { get; set; }
    }
}
