﻿using TechTest.Common.Responses;
using TechTest.Services.Dtos;

namespace TechTest.Services.Responses
{
    public class ResponseGetUserLogin: Response
    {
        public UserDto User {get; set;}
    }
}
