﻿using System.Collections.Generic;
using TechTest.Common.Responses;
using TechTest.Services.Dtos;

namespace TechTest.Services.Responses
{
    public class ResponseGetJobsByTitle: Response
    {
        public List<JobDto> Jobs { get; set; }
    }
}
