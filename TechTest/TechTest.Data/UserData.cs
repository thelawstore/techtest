﻿using TechTest.Common.Utilities;
using System.Linq;
using System.Security.Authentication;
using TechTest.Common.Requests;
using TechTest.Data.Daos;

namespace TechTest.Data
{
    public class UserData
    {
        public User GetUserLogin(RequestGetUserLogin request)
        {
            var user = new User();

            string encryptedPassword = Encryptor.Encrypt(request.Password);

            using (var context = new DbContextTechTest())
            {
                user = context.Users.FirstOrDefault(u =>
                u.UserId == request.UserId && u.Password == encryptedPassword);
            }

            if (user == null)
                throw new AuthenticationException("Invalid username or password.");

            return user;
        }
    }
}
