﻿using System.ComponentModel.DataAnnotations;

namespace TechTest.Common.Requests
{
    public class RequestGetUserLogin : Request
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a username.")]
        public string UserId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a password.")]
        public string Password { get; set; }
    }
}
