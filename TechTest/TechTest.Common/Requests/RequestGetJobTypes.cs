﻿namespace TechTest.Common.Requests
{
    /// <summary>
    /// Retrieves all job types from DB.
    /// This is just an empty request as no validation or search criteria
    /// is required.
    /// </summary>
    public class RequestGetJobTypes: Request
    {
    }
}
