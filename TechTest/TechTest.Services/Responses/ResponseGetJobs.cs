﻿using System.Collections.Generic;
using TechTest.Common.Responses;
using TechTest.Services.Dtos;

namespace TechTest.Services.Responses
{
    public class ResponseGetJobs: Response
    {
        public List<JobDto> Jobs { get; set; }
    }
}
