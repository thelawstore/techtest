﻿using System;
using TechTest.Common.Requests;
using TechTest.Common.Responses;
using TechTest.Services.Responses;
using TechTest.Data;
using TechTest.Data.Daos;
using TechTest.Services.Dtos;

using System.Net;

namespace TechTest.Services
{
    public class UserService
    {
        public ResponseGetUserLogin GetUserLogin(RequestGetUserLogin request)
        {
            var response = new ResponseGetUserLogin();

            // Make sure username and password aren't empty before querying.
            response = (ResponseGetUserLogin)request.CheckValidation(response);

            if (response.Status == HttpStatusCode.BadRequest)
                return response;

            try
            {
                var data = new UserData();
                UserDto userDto = new(data.GetUserLogin(request));

                response.User = userDto;

                response.Status = HttpStatusCode.OK;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.OK, "User login successful."));
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.StatusMessages.Add(new StatusMessage(
                    HttpStatusCode.InternalServerError, ex.Message));
            }

            return response;
        }
    }
}
