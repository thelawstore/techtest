﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace TechTest.Common.Requests
{
    public class RequestAddJob: Request
    {
        [Required(ErrorMessage = "You must select a job type.")]
        public int JobTypeId { get; set; }

        [Required(ErrorMessage = "You must enter a salary.")]
        public decimal Salary { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a title.")]
        [StringLength(32, MinimumLength = 1, ErrorMessage = "Title must be between {2} and {1} characters long.")]
        public string Title { get; set; }

        // Can be empty.
        [StringLength(1024, MinimumLength = 0, ErrorMessage = "Description cannot exceed {2} characters.")]
        public string Description { get; set; }
    }
}
