﻿import React, { Component } from 'react';
import { JobGrid } from './sub/JobGrid';

export class Jobs extends Component {
    static displayName = Jobs.name;

    render() {
        return (
            <div>
                <h1>Jobs</h1>
                <p>Please review our job listings below.</p>
                <JobGrid />
            </div>
        );
    }
}