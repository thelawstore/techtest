import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Jobs } from './components/Jobs';
import { Account } from './components/Account';

import './custom.css'

export default class App extends Component {
static displayName = App.name;

    render () {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route exact path='/jobs' component={Jobs} />
                <Route exact path='/account' component={Account} />
            </Layout>
        );
    }
}
