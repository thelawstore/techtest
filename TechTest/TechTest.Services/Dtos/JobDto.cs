﻿using TechTest.Data.Daos;

namespace TechTest.Services.Dtos
{
    public class JobDto
    {
        public int JobId { get; set; }
        public JobTypeDto JobType { get; set; }
        public decimal Salary { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Default constructor. Takes no arguments.
        /// </summary>
        public JobDto()
        {
        }

        /// <summary>
        /// Overloaded constructor that takes a Job object and converts it to
        /// a JobDto object.
        /// </summary>
        /// <param name="job"></param>
        public JobDto(Job job)
        {
            JobTypeDto jobType = new(job.JobType);

            JobId = job.JobId;
            JobType = jobType;
            Salary = job.Salary;
            Title = job.Title;
            Description = job.Description;
        }

        /// <summary>
        /// Converts this JobDto to a Job object and returns it.
        /// </summary>
        /// <returns></returns>
        public Job ToDao()
        {
            var result = new Job()
            {
                JobId = JobId,
                JobTypeId = JobType.JobTypeId,
                Salary = Salary,
                Title = Title,
                Description = Description,
            };

            return result;
        }
    }
}
